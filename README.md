# M4 Forecasting

## Remark:

All the datasets should be concluded in a folder named
**raw-data**.

## Timeline

* 29-01-2018: creation of repository 

## TO DO

* train-test splitting (bootstrap)


## Refs

* [M4 Forecasting Official Site](https://www.m4.unic.ac.cy/)

* [Competition Guide](https://www.m4.unic.ac.cy/wp-content/uploads/2017/12/M4-Competitors-Guide.pdf)

* [Benchmark](https://github.com/M4Competition/M4-methods)
